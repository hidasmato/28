const express = require('express')
const path = require('path')
const fs = require('fs')
const app = express()
const port = 3000
const morgan = require('morgan')
const Pool = require('pg').Pool


const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));


const pool = new Pool({
    user: "game_administrator",
    host: "localhost",
    password: "1234567890",
    database: "users",
    port: 5432
});

app.use(express.json())
app.set('view engine', 'ejs');
const createPath = (page) => { return path.resolve(__dirname, 'views', `${page}.ejs`) }


//app.use(express.static('public')) //Добавление папки из которой можно передавать данные

//Логгирование запросов 
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))

app.get('/', async (req, res) => {
    try {
        const cursor = await pool.query('SELECT * FROM users');
        res.render(createPath("index"), { users: cursor.rows })
    } catch (error) {
        return res.status(500).json('Internal Server error');
    }
})


//Проверка ошибок
app.use("/users/change/*", (req, res, next) => {
    const { name, color } = req.body;
    if (!name || (req.path == '/users/change/add' && !color)) {
        res.status(400).send("Неправильные данные");
    }
    else
        next();
})



app.post('/users/change/add', async (req, res) => {
    try {
        const cursor = (await pool.query('SELECT * FROM users WHERE name = $1', [req.body.name])).rows;
        if (cursor.length != 0) {
            res.status(400).send("Пользователь существует");
        }
        else {
            await pool.query('INSERT INTO users (name, color) VALUES ($1, $2);', [req.body.name, req.body.color]);
            res.redirect('/')
        }
        res.render(createPath("index"), { users: cursor.rows })
    } catch (error) {
        return res.status(500).json('Internal Server error');
    }
})

app.post('/users/change/delete', async (req, res) => {
    try {
        const cursor = (await pool.query('SELECT * FROM users WHERE name = $1', [req.body.name])).rows;
        if (cursor.length == 0) {
            res.status(400).send("Пользователь не существует");
        }
        else {
            await pool.query('DELETE FROM public.users WHERE name = $1', [req.body.name]);
            res.redirect('/')
        }
    } catch (error) {
        return res.status(500).json('Internal Server error');
    }
})
app.post('/users/change/update', async (req, res) => {
    try {
        const cursor = (await pool.query('SELECT * FROM users WHERE name = $1', [req.body.name])).rows;
        if (cursor.length == 0) {
            res.status(400).send("Пользователь не существует");
        }
        else {
            await pool.query('UPDATE public.users SET color = $1 WHERE name = $2', [req.body.color, req.body.name]);
            res.redirect('/')
        }
    } catch (error) {
        return res.status(500).json('Internal Server error');
    }
})
app.listen(port, () => {
    console.log(`Example app listening on port http://localhost:${port}`)
})

